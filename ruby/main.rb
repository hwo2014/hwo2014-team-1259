require 'json'
require 'socket'

# 引数
server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]
track = ARGV[4] # トラック名
car = ARGV[5] # 車の数

$bot_name_g = bot_name

#puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"
puts "pieceIndex, inPieceDistance, throttle, acceleration, speed, angle, angularVelocity, startLaneIndex, lap"

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key, track, car)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp, track, car)
  end

  private

  def play(bot_name, bot_key, tcp, track, car)
    tcp.puts join_message(bot_name, bot_key, track, car)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    previousAngle = 0
    previousPieceIndex = 0
    previousInPieceDistance = 0
    previousSpeed = 0
    angularVelocity = 0 # 角速度

    # カーブ危険度の計算用
    bendFactor = 0
    bendFactorThreshold = 0
    tmp_radius = 0
    tmp_angle = 0
    pieceLookAhead = 0 #何ピース先読みするかを保存しておく

    # Turbo関連
    isTurboAvailable = false
    turboDurationMilliseconds = 0
    turboDurationTicks = 0
    turboFactor = 0
    isTurboUse = false

    piecesData = Array.new() # piecesが入ってる Hashでアクセス
    lanesData = Array.new() # lanesが入ってる
    

    while json = tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']

      myTeamId = 0
      angle = 0
      speed = 0
      acceleration = 0
      pieceIndex = 0
      inPieceDistance = 0
      startLaneIndex = 0
      endLaneIndex = 0
      distanceFromCenter = 0
      lap = 0

      throttle = 0

      case msgType
        when 'carPositions'
          # 各種情報の保存
          gameId = message['gameId']
          gameTick = message['gameTick']
          
          teams = Array.new()
          msgData.each do |team|
            teams.push(team)
          end

          teamData = Array.new()
          teams.each do |tmp|
            teamData.push(tmp)
          end

          # 自チームの判定
          for i in 0..(teamData.size - 1) do
            if (teamData[i]['id']['name'] == $bot_name_g)
              myTeamId = i
            end
          end

          # 現在の状況 carpositions の中身
          angle = teams[myTeamId]['angle']
          pieceIndex = teams[myTeamId]['piecePosition']['pieceIndex']
          inPieceDistance = teams[myTeamId]['piecePosition']['inPieceDistance']
          startLaneIndex = teams[myTeamId]['piecePosition']['lane']['startLaneIndex']
          endLaneIndex = teams[myTeamId]['piecePosition']['lane']['endLaneIndex']
          distanceFromCenter = lanesData[startLaneIndex]['distanceFromCenter'] #startLaneでいいのか?(誤差の範疇か?)
          lap = teams[myTeamId]['piecePosition']['lap']


          # 車の速度の計算 previousPieceIndexがカーブだと少し面倒
          if (pieceIndex == previousPieceIndex)
          	# 同じピース内なので単純に距離の差分
            speed = inPieceDistance - previousInPieceDistance
          else
          	# ピースをまたぐとき
          	if(piecesData[previousPieceIndex]['length'] != nil)
              # previous がストレート
          		speed = inPieceDistance + (piecesData[previousPieceIndex]['length'] - previousInPieceDistance)
          	else
          		# previous がカーブ バグ持ち
              #speed = inPieceDistance + (calc_bend_length(distanceFromCenter, piecesData[previousPieceIndex]['radius'], piecesData[previousPieceIndex]['angle']) - previousInPieceDistance)
              speed = previousSpeed
            end
          end

          angularVelocity = angle - previousAngle
          acceleration = speed - previousSpeed

          # lengthが400を超えるまで先読みをするため、カウント
          if(pieceIndex != previousPieceIndex)
            i = 0
            sumOfPiecesLength = 0
            while sumOfPiecesLength < 400
              if(piecesData[(pieceIndex + i)%piecesData.length]['length'] != nil)
                sumOfPiecesLength += piecesData[(pieceIndex + i)%piecesData.length]['length']
              else
                sumOfPiecesLength += calc_bend_length(distanceFromCenter,
                piecesData[(pieceIndex + i)%piecesData.length]['radius'], piecesData[(pieceIndex + i)%piecesData.length]['angle'])
              end
              i += 1
            end
            pieceLookAhead = i
          end

          # ピースの先読みをしてbendFactorを求める
          bendFactor = 0
          for i in 0..pieceLookAhead do
            if(piecesData[(pieceIndex + i)%piecesData.length]['length'] == nil)
              tmp_radius = piecesData[(pieceIndex + i)%piecesData.length]['radius']
              tmp_angle = piecesData[(pieceIndex + i)%piecesData.length]['angle']
              bendFactor += ((speed*speed)/(tmp_radius * tmp_angle.abs/360)) * (pieceLookAhead - i)
            end
          end

          #bendFactorの設定
          if(bendFactorThreshold == 0 && angle > 1)
            bendFactorThreshold = bendFactor/2
            puts "bendFactorThreshold = #{bendFactorThreshold}"
          end

          # bendfactorThreshold を超えるとスロットルの減速
          if(bendFactor < bendFactorThreshold)
            throttle = 1
          else
            throttle = 1 - bendFactor/100 - angle.abs/10
            if(throttle < 0)
              throttle = 0
            end
          end

          # turboの使いどころのチェック
          if(isTurboAvailable)
            isTurboUse = true
            for i in 2..5 do
              if(piecesData[(pieceIndex + i)%piecesData.length]['length'] == nil)
                isTurboUse = false
              end
              if(bendFactor > 20)
                isTurboUse = false
              end
            end
          end

          # 今のピースがどんなのか表示
          if(pieceIndex != previousPieceIndex)
            puts
            puts "pieceIndex = #{pieceIndex}"
            if(piecesData[pieceIndex]['length'] != nil)
              puts "length = #{piecesData[pieceIndex]['length']}"
            else
              puts "radius = #{piecesData[pieceIndex]['radius']}, angle = #{piecesData[pieceIndex]['angle']}"
            end
          end

          # throttleかturboのどちらかしか送れない？
          if(isTurboUse)
            throttle = 0
            tcp.puts turbo_message()
            isTurboUse = false
            isTurboAvailable = false
          else
            tcp.puts throttle_message(throttle)
          end

          puts "BF = #{bendFactor}, angle = #{angle}, speed = #{speed}, iPD = #{inPieceDistance}, throttle = #{throttle}"

          # ピース番号, ピース中の車の位置, スロットル, 加速度, 速度, 角度, 角速度, レーン番号, ラップs
          #puts "#{pieceIndex},#{inPieceDistance},#{throttle},#{acceleration},#{speed},#{angle},#{angularVelocity},#{startLaneIndex},#{lap}"


          previousAngle = angle
          previousSpeed = speed
          previousPieceIndex = pieceIndex
          previousInPieceDistance = inPieceDistance
          #ここまで

        else
          case msgType
            when 'join'
              puts 'Joined'
            when 'gameStart'
              puts 'Race started'
            when 'turboAvailable'
              isTurboAvailable = true
              turboDurationMilliseconds = msgData['turboDurationMilliseconds']
              turboDurationTicks = msgData['turboDurationTicks']
              turboFactor = msgData['turboFactor']
              puts "turbo get message"
            when 'crash'
              puts 'Someone crashed'
              #tcp.close
              #return
            when 'gameEnd'
              #puts 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
            when 'gameInit'
            	#ネストしたJSONからのデータの取り出しがよくわからないけど通る
            	msgData['race']['track']['pieces'].each_with_index do |tmp, i| # tmp->"race"
            		piecesData.push(tmp)
                puts tmp
            	end
              msgData['race']['track']['lanes'].each_with_index do |tmp, i| # tmp->"race"
                lanesData.push(tmp)
                puts tmp
              end
              #puts "#{msgData}"
          end
          puts "Got #{msgType}"
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key, track, car)
  	# Continuous Integration ではjoinのみ有効らしい
    make_msg("join", {:name => bot_name, :key => bot_key})
    #make_msg("joinRace", {:botId => {:name => bot_name, :key => bot_key}, :trackName => track, :carCount => car})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def turbo_message()
    make_msg("turbo", "Pow pow pow pow pow, or your of personalized turbo message")
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  def calc_bend_length(distanceFromCenter, radius, angle)
    return 2*Math::PI*(radius+distanceFromCenter)*((angle.abs)/360)
  end

end

NoobBot.new(server_host, server_port, bot_name, bot_key, track, car)
